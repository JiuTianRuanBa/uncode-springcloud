package cn.uncode.springcloud.admin.dal.system;

import org.springframework.stereotype.Service;

import cn.uncode.springcloud.admin.model.system.dto.GatewayRouteDTO;

import cn.uncode.dal.external.AbstractCommonDAL;

 /**
 * service类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-05-28
 */
@Service
public class GatewayRouteDALImpl extends AbstractCommonDAL<GatewayRouteDTO> implements GatewayRouteDAL {

}