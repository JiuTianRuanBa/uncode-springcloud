package cn.uncode.springcloud.admin.model.system.dto;

import java.util.Date;

import cn.uncode.dal.annotation.Field;
import cn.uncode.dal.annotation.Table;
import lombok.Data;
/**
 * 数据库实体类,此类由Uncode自动生成
 * @author uncode
 * @date 2019-05-28
 */
@Data
@Table(name ="config")
public class ConfigDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5336672053753462850L;
	
	public final static String TABLE_NAME = "config";
	public final static String ID = "id";
	public final static String NAME = "name";
	public final static String KEY = "key";
	public final static String VALUE = "value";
	public final static String TYPE = "type";
	public final static String CREATE_TIME = "create_time";
	public final static String UPDATE_TIME = "update_time";
	public final static String REMARK = "remark";

	/**
	 * 主键ID
	 */
	private Long id;
	/**
	 * 参数名称
	 */
	private String name;
	/**
	 * 参数键名
	 */
	private String key;
	/**
	 * 参数键值
	 */
	private String value;
	/**
	 * 状态（1:系统级，2：功能级，3：其他）
	 */
	private int type;
	/**
	 * 创建时间
	 */
	 @Field(name = "create_time")
	private Date createTime;
	/**
	 * 更新时间
	 */
	 @Field(name = "update_time")
	private Date updateTime;
	/**
	 * 备注
	 */
	private String remark;


}